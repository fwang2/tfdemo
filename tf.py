#!/usr/bin/env python
# assume this is tensorflow program to be called for externally
#   IN: (1) rank id (2) input value
#       rank id is not really needed except for demo printout to make it clear
#
#   OUT: value + random

from __future__ import print_function
import sys
import random
import time

if len(sys.argv) < 2:
    raise RuntimeError("We need two input value: rank and a number")

rank = int(sys.argv[1])
input = float(sys.argv[2])
out = input + random.randint(0, 100)  # okay, output depends on input

# pretend to sleep random amount of time (between 1 and 5 secs
# to simulate different processing speed of TF

naptime = random.randint(1, 5)
print("Rank %s will sleep %s seconds" % (rank, naptime))
time.sleep(naptime)


# the output is result, output some text to simulate you have
# to parse and extract the value "out"

print("Rank %s in: %s out: %s" % (rank, input, out))


