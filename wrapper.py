#!/usr/bin/env python
#
# A wrapper for making TensorFlow (TF) MPI-alike
#
#
from __future__ import print_function
from mpi4py import MPI
import subprocess
import random
import re

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


def run_tf(in_val):
    """Launch tf.py as external program"""
    args = ["tf.py", "%s" % rank, "%s" % in_val]
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()

    if stderr:
        raise RuntimeError(stderr)
    elif stdout:
        # we parse the output
        print(stdout)
        pattern = re.compile("out:.*") # a crude way of parsing
        return float(pattern.findall(stdout)[0].split()[1])

    else:
        raise RuntimeError("Unable to get TF result")



# now the main flow

if rank == 0:
    print("You launched a total of [%s] processes" % size)

avg = random.randrange(0,100) # start with a random

for _ in xrange(2):

    val = run_tf(avg)
    print("Rank %s: TF return: %s" % (rank, val))

    sum = comm.allreduce(val, op=MPI.SUM)

    # update avg input with the new average
    avg = sum/size

    print("Rank %s, avg = %s" % (rank, avg) )




